import { Component, ViewChild } from '@angular/core';
import { SwiperComponent } from 'swiper/angular';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.sass']
})
export class HomeComponent {
	@ViewChild('swiper', { static: false }) swiper?: SwiperComponent;

	slideNext = () => {
		this.swiper?.swiperRef.slideNext();
	}

	slidePrev = () => {
		this.swiper?.swiperRef.slidePrev();
	}
}
