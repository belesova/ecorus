import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
	selector: 'app-profile-history',
	templateUrl: './profile-history.component.html',
	styleUrls: ['./profile-history.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileHistoryComponent {
	historyArr: Array<any> = [
		{
			material: '',
			date: '25.09.2021',
			address: 'Казань, Кремлёвская, 88',
			cost: '1000'
		},
		{
			material: '',
			date: '25.09.2021',
			address: 'Казань, Кремлёвская, 88',
			cost: '1000'
		},
		{
			material: '',
			date: '25.09.2021',
			address: 'Казань, Кремлёвская, 88',
			cost: '1000'
		},
		{
			material: '',
			date: '25.09.2021',
			address: 'Казань, Кремлёвская, 88',
			cost: '1000'
		},
	]
}
