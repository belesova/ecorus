import { ChangeDetectorRef, Component } from '@angular/core';
import { BottomSheetService } from '../../services/bottom-sheet.service';
import { CollectionPointsFiltersComponent } from '../../components/BottomSheetElements/collection-points-filters/collection-points-filters.component';
import { CardsListCollectionPointsComponent } from '../../components/cards-list-collection-points/cards-list-collection-points.component';
import { ComponentPortal, Portal } from '@angular/cdk/portal';
import { ItemCollectionPointComponent } from '../../components/ui/item-collection-point/item-collection-point.component';
import { ListCollectionPointsComponent } from '../../components/BottomSheetElements/list-collection-points/list-collection-points.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-collection-points',
	templateUrl: './collection-points.component.html',
	styleUrls: ['./collection-points.component.sass']
})
export class CollectionPointsComponent {
	arrMaterials = ['Стекло', 'Бумага', 'Металл']
	arrShops = ['H&M', 'P&B', 'Adidas']

	selectedPortal!: Portal<any>;
	cardPortal!: ComponentPortal<any>;
	listCardPortal!: ComponentPortal<any>;

	constructor(private bottomSheetService: BottomSheetService,
				private route: ActivatedRoute,
				private cdk: ChangeDetectorRef) {

		this.route.queryParams.subscribe(value => {
			if (value['idCollectionPointCard']) {
				this.selectedPortal = this.cardPortal
			} else {
				this.selectedPortal = this.listCardPortal
			}
		})
	}

	openBottomSheetFilters(): void {
		this.bottomSheetService.openDialog(CollectionPointsFiltersComponent)
	}

	openBottomSheetListCollectionPoint() {
		this.bottomSheetService.openDialog(ListCollectionPointsComponent)
	}

	ngAfterViewInit(): void {
		this.cardPortal = new ComponentPortal<any>(ItemCollectionPointComponent)
		this.listCardPortal = new ComponentPortal<any>(CardsListCollectionPointsComponent)
		this.selectedPortal = this.listCardPortal
		this.cdk.detectChanges()
	}
}
