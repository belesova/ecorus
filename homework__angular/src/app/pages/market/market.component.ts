import { Component, Input, TemplateRef, ViewChild } from '@angular/core';
import { MarketService, Product } from '../../services/market.service';
import { BottomSheetService } from '../../services/bottom-sheet.service';
import { MarketFiltersBottomSheetComponent } from '../../components/BottomSheetElements/market-filters-bottom-sheet/market-filters-bottom-sheet.component';
import { rotationAnimation } from '../../animation/rotation-animation';

export interface CardData {
	state: 'default' | 'flipped' | 'matched';
	data: Product;
}

@Component({
	selector: 'app-market',
	templateUrl: './market.component.html',
	styleUrls: ['./market.component.sass'],
	animations: [
		rotationAnimation
	]
})
export class MarketComponent {
	@ViewChild('templateBottomSheet') TemplateBottomSheet?: TemplateRef<any>;
	@Input() cardProducts: Array<CardData> = [];
	arrProducts: Array<Product> = this.marketService.getGoods();
	headersFilters: Array<string> = ['По популярности', 'По цене', 'По новизне']
	genderFilters: Array<string> = ['Мужской', 'Женский']
	categoryFilters: Array<string> = ['Одежда', 'Обувь', 'Аксессуары']
	brandFilters: Array<string> = ['H&M', 'P&B', 'Adidas', 'Nike', 'Rebook', 'H&M', 'P&B', 'Adidas', 'Nike', 'Rebook', 'H&M', 'P&B', 'Adidas', 'Nike', 'Rebook']


	constructor(private marketService: MarketService, private bottomSheetService: BottomSheetService) {
		this.arrProducts.map(product => this.cardProducts.push({
			state: 'default',
			data: product
		}))
	}

	openBottomSheetFilters() {
		this.bottomSheetService.openDialog(MarketFiltersBottomSheetComponent)
	}


	cardClicked(value: CardData) {
		if (value.state === 'default') {
			value.state = 'flipped';
		} else {
			value.state = 'default';
		}
	}
}
