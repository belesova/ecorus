import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
	selector: 'app-promocodes',
	templateUrl: './promocodes.component.html',
	styleUrls: ['./promocodes.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PromocodesComponent {
	promocodes: Array<any> = [
		{
			date: '25.09.2021',
			link: 'adidas.com/clothes/WddfJfsf7dt6fsHFIuj5пdfsZFu...',
		},
		{
			date: '25.09.2021',
			link: 'adidas.com/clothes/WddfJfsf7dt6fsHFIuj5пdfsZFu...',
		},
		{
			date: '25.09.2021',
			link: 'adidas.com/clothes/WddfJfsf7dt6fsHFIuj5пdfsZFu...',
		},
	]
}
