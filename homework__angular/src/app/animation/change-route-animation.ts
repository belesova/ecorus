import { animate, animateChild, group, query, style, transition, trigger } from '@angular/animations';

export const routeChangeAnimation = trigger(
	'routeChangeAnimation',
	[
		transition('page1 <=> page2', [
			style({ position: 'relative' }),
			query(':enter, :leave', [
				style({
					position: 'absolute',
					top: 80,
					left: 0,
					width: '100%',
				}),
			]),
			query(':enter', [style({ left: '-100%' })]),
			query(':leave', animateChild()),
			group([
				query(':leave', [
					animate(
						'300ms ease-out',
						style({ left: '100%' })
					),
				]),
				query(':enter', [
					animate('300ms ease-out', style({ left: '0%' })),
				]),
			]),
			query(':enter', animateChild()),
		]),
	]
)
