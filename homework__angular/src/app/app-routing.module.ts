import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { MarketComponent } from './pages/market/market.component';
import { CollectionPointsComponent } from './pages/collection-points/collection-points.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { AuthenticationGuard } from './guards/authentication.guard';
import { AboutServiceComponent } from './pages/about-service/about-service.component';
import { ProfileHistoryComponent } from './pages/profile-history/profile-history.component';
import { PromocodesComponent } from './pages/promocodes/promocodes.component';
const routes: Routes = [
	{
		component: HomeComponent,
		path: '',
		data: { animation: 'page1' }
	},
	{
		component: MarketComponent,
		path: 'market',
		data: { animation: 'page2' }
	},
	{
		component: CollectionPointsComponent,
		path: 'collection-points',
		data: { animation: 'page2' },
	},
	{
		component: AboutServiceComponent,
		path: 'about-service',
		data: { animation: 'page2' }
	},
	{
		component: ProfileComponent,
		path: 'profile',
		canActivate: [AuthenticationGuard],
		children: [
			{
				component: ProfileHistoryComponent,
				path: 'history'
			},
			{
				component: PromocodesComponent,
				path: 'promocodes'
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
