export interface GoodModel {
	shopName: string,
	item_id: string,
	item_name: string,
	photo_url: string,
	description: string,
	price: 0
}
