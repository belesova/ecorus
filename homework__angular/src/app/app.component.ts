import { Component } from '@angular/core';
import { routeChangeAnimation } from './animation/change-route-animation';
import { RouterOutlet } from '@angular/router';

@Component({
	selector: 'tk-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass'],
	animations: [routeChangeAnimation],
})
export class AppComponent {
	title = 'homework__angular';

	getRouteAnimationState(outlet: RouterOutlet) {
		return (
			outlet &&
			outlet.activatedRouteData &&
			outlet.activatedRouteData['animation']
		)
	}

}
