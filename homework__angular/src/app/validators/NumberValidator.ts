import { FormControl } from '@angular/forms';

export const numberValidator = (control: FormControl) => {
	const validLength = control.value.length === 16;
	if (!validLength) {
		return { validNumber: false}
	}
	return null;
}
