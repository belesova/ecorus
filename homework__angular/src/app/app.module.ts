import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { CardPageComponent } from './components/ui/card-page/card-page.component';
import { MarketComponent } from './pages/market/market.component';
import { CollectionPointsComponent } from './pages/collection-points/collection-points.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AboutServiceComponent } from './pages/about-service/about-service.component';
import { ButtonWithIconComponent } from './components/ui/button-with-icon/button-with-icon.component';
import { IconComponent } from './components/ui/icon/icon.component';
import { SwiperModule } from 'swiper/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CheckboxGroupComponent } from './components/ui/checkbox-group/checkbox-group.component';
import { CheckboxComponent } from './components/ui/checkbox/checkbox.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PortalModule } from '@angular/cdk/portal';
import { OverlayModule } from '@angular/cdk/overlay';
import { AuthFormComponent } from './components/forms/auth-form/auth-form.component';
import { InputComponent } from './components/ui/input/input.component';
import { ButtonComponent } from './components/ui/button/button.component';
import { ModalContainerComponent } from './components/modal-container/modal-container.component';
import { DialogModule } from '@angular/cdk-experimental/dialog';
import { RegistrationFormComponent } from './components/forms/registration-form/registration-form.component';
import { SlideSwiperComponent } from './components/ui/slide-swiper/slide-swiper.component';
import { AuthSmsFormComponent } from './components/forms/auth-sms-form/auth-sms-form.component';
import { AuthForCompanyComponent } from './components/forms/auth-for-company/auth-for-company.component';
import { CardProductComponent } from './components/ui/card-product/card-product.component';
import { MapComponent } from './components/map/map.component';
import { InputWithIconComponent } from './components/ui/input-with-icon/input-with-icon.component';
import { CollectionPointCardComponent } from './components/ui/collection-point-card/collection-point-card.component';
import { ProfileHistoryComponent } from './pages/profile-history/profile-history.component';
import { HistoryCardComponent } from './components/ui/history-card/history-card.component';
import { PromocodesComponent } from './pages/promocodes/promocodes.component';
import { PromocodeComponent } from './components/ui/promocode/promocode.component';
import { IMaskModule } from 'angular-imask';
import { SelectorComponent } from './components/ui/selector/selector.component';
import { CardsListCollectionPointsComponent } from './components/cards-list-collection-points/cards-list-collection-points.component';
import { ButtonPrevPageComponent } from './components/ui/button-prev-page/button-prev-page.component';
import { MenuComponent } from './components/menu/menu.component';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { LayoutContainerComponent } from './containers/layout-container/layout-container.component';
import { ItemCollectionPointComponent } from './components/ui/item-collection-point/item-collection-point.component';
import { BottomSheetComponent } from './components/bottom-sheet/bottom-sheet.component';
import { MarketFiltersBottomSheetComponent } from './components/BottomSheetElements/market-filters-bottom-sheet/market-filters-bottom-sheet.component';
import { CollectionPointsFiltersComponent } from './components/BottomSheetElements/collection-points-filters/collection-points-filters.component';
import { WrapperBottomSheetComponent } from './components/BottomSheetElements/wrapper-bottom-sheet/wrapper-bottom-sheet.component';
import { ListCollectionPointsComponent } from './components/BottomSheetElements/list-collection-points/list-collection-points.component';
import { CdkComboboxModule } from '@angular/cdk-experimental/combobox';
import { BalanceCardComponent } from './components/balance-card/balance-card.component';
import { QRModalComponent } from './components/modals/qrmodal/qrmodal.component';
import { ErrorInterceptorService } from './services/interceptors/error-interceptor.service';
import { AuthInterceptorService } from './services/interceptors/auth-interceptor.service';
import { UrlInterceptorService } from './services/interceptors/url-interceptor.service';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		CardPageComponent,
		MarketComponent,
		CollectionPointsComponent,
		ProfileComponent,
		HeaderComponent,
		FooterComponent,
		AboutServiceComponent,
		ButtonWithIconComponent,
		IconComponent,
		CheckboxGroupComponent,
		CheckboxComponent,
		AuthFormComponent,
		InputComponent,
		ButtonComponent,
		ModalContainerComponent,
		RegistrationFormComponent,
		SlideSwiperComponent,
		AuthSmsFormComponent,
		AuthForCompanyComponent,
		CardProductComponent,
		MapComponent,
		InputWithIconComponent,
		CollectionPointCardComponent,
		ProfileHistoryComponent,
		HistoryCardComponent,
		PromocodesComponent,
		PromocodeComponent,
		SelectorComponent,
		CardsListCollectionPointsComponent,
		ButtonPrevPageComponent,
		MenuComponent,
  LayoutContainerComponent,
  ItemCollectionPointComponent,
  BottomSheetComponent,
  MarketFiltersBottomSheetComponent,
  CollectionPointsFiltersComponent,
  WrapperBottomSheetComponent,
  ListCollectionPointsComponent,
  BalanceCardComponent,
  QRModalComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		SwiperModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		PortalModule,
		OverlayModule,
		DialogModule,
		IMaskModule,
		MatBottomSheetModule,
		CdkComboboxModule
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: UrlInterceptorService,
			multi: true,
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptorService,
			multi: true,
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: ErrorInterceptorService,
			multi: true
		}
	],
	bootstrap: [AppComponent],
})
export class AppModule {
}
