import { Component } from '@angular/core';
import { DialogService } from '../../services/dialog.service';
import { AuthFormComponent } from '../forms/auth-form/auth-form.component';
import { MenuComponent } from '../menu/menu.component';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.sass']
})
export class HeaderComponent {
	userAuth = window.localStorage.getItem('token');

	constructor(private dialogService: DialogService) {
	}

	openModal() {
		this.dialogService.openDialog(AuthFormComponent, {})
	}

	openMenu() {
		this.dialogService.openDialog(MenuComponent, {})
	}
}
