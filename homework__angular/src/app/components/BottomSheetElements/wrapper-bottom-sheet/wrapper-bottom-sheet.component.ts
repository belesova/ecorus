import { Component, ChangeDetectionStrategy } from '@angular/core';
import { BottomSheetService } from '../../../services/bottom-sheet.service';

@Component({
  selector: 'app-wrapper-bottom-sheet',
  templateUrl: './wrapper-bottom-sheet.component.html',
  styleUrls: ['./wrapper-bottom-sheet.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WrapperBottomSheetComponent{
	constructor(private bottomSheetService: BottomSheetService) {
	}

	closeBottomSheet() {
		this.bottomSheetService.closeDialog()
	}
}
