import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-item-collection-point',
	templateUrl: './iterm-collection-point.component.html',
	styleUrls: ['./item-collection-point.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemCollectionPointComponent {
	idCard: string = this.route.snapshot.queryParamMap.get('idCollectionPointCard')!;

	materials: Array<any> = [
		{
			title: 'Пластик',
			count: 5
		},
		{
			title: 'Стекло',
			count: 2
		},
		{
			title: 'Бумага',
			count: 10
		},
		{
			title: 'Батареи',
			count: 2
		},
	]

	shops: Array<any> = [
		{
			title: 'H&M',
			points: this.materials
		},
		{
			title: 'Adidas',
			points: this.materials
		},
	]

	constructor(private route: ActivatedRoute) {
	}
}
