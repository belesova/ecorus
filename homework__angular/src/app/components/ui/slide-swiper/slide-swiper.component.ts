import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-slide-swiper',
	templateUrl: './slide-swiper.component.html',
	styleUrls: ['./slide-swiper.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlideSwiperComponent {
	@Input() title!: string;
	@Input() subtitle!: string;
	@Input() btn!: string;
	@Input() img!: string;
	@Input() back__color: '' | 'BLUE' | 'YELLOW' = '';
	@Input() link: string = '';

	constructor(private sanitizer: DomSanitizer) {
		sanitizer.bypassSecurityTrustHtml(this.title)
		sanitizer.bypassSecurityTrustHtml(this.subtitle)
	}
}
