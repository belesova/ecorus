import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-collection-point-card',
	templateUrl: './collection-point-card.component.html',
	styleUrls: ['./collection-point-card.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CollectionPointCardComponent {
	@Input() img!: string;
	@Input() address!: string;
	@Input() materials!: string;

	constructor(private sanitizer: DomSanitizer) {
		sanitizer.bypassSecurityTrustHtml(this.address)
	}
}
