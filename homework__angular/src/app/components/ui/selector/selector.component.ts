import { Component, ChangeDetectionStrategy, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
	selector: 'app-selector',
	templateUrl: './selector.component.html',
	styleUrls: ['./selector.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectorComponent {
	@Input() itemsName!: string;
	@Input() arrSelect!: Array<string>
	isOpen: boolean = false

	@ViewChild('origin') origin!: ElementRef;
	originRect!: ClientRect;

	changeOpen() {
		this.originRect = this.origin.nativeElement?.getBoundingClientRect()
		this.isOpen = !this.isOpen
	}
}
