import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-promocode',
	templateUrl: './promocode.component.html',
	styleUrls: ['./promocode.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PromocodeComponent {
	@Input() date!: string;
	@Input() link!: string;

	constructor(private sanitizer: DomSanitizer) {
		sanitizer.bypassSecurityTrustHtml(this.date)
		sanitizer.bypassSecurityTrustHtml(this.link)
	}
}
