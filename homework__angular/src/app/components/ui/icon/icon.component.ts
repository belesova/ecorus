import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
	selector: 'app-icon',
	templateUrl: './icon.component.html',
	styleUrls: ['./icon.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	host: {
		'[style.width]': 'this.size',
		'[style.height]': 'this.size',
	}
})
export class IconComponent {
	@Input() name: string = '';
	@Input() size: string = '24px';
}
