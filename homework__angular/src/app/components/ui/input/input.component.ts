import { Component, ChangeDetectionStrategy, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-input',
	templateUrl: './input.component.html',
	styleUrls: ['./input.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => InputComponent),
		multi: true
	}]

})
export class InputComponent implements ControlValueAccessor {
	@Input() placeholder: string = '';
	@Input() invalid: boolean = false;
	@Input() type: 'text' | 'number' | 'password' = 'text';
	@Input() hasMaskNumber: boolean = false;

	constructor(private sanitizer: DomSanitizer) {
		sanitizer.bypassSecurityTrustHtml(this.placeholder)
	}

	input: FormControl = new FormControl('')

	maskVal = {
		mask: '+7{(}900{)}000{ }00{ }00'
	}

	onChange: any = (value: any) => {
		this.input.setValue(value);
		this.onChangeCallback(this.input.value);
		this.onTouchedCallback();
	}

	onChangeCallback = (v: any) => {
	};
	onTouchedCallback = () => {
	};


	writeValue(value: any) {
		this.input.setValue(value)
	}

	registerOnChange(fn: any): void {
		this.onChangeCallback = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouchedCallback = fn;
	}
}
