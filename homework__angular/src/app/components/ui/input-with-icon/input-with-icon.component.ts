import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ControlValueAccessor, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-input-with-icon',
	templateUrl: './input-with-icon.component.html',
	styleUrls: ['./input-with-icon.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputWithIconComponent implements ControlValueAccessor {
	@Input() placeholder: string = '';
	@Input() invalid: boolean = false;
	@Input() type: 'text' | 'number' | 'password' = 'text';
	@Input() hasMaskNumber: boolean = false;
	@Input() iconName!: string;

	input: FormControl = new FormControl('')

	maskVal = {
		mask: '00{:}00{:}0000000{:}0'
	}

	constructor(private sanitizer: DomSanitizer) {
		sanitizer.bypassSecurityTrustHtml(this.placeholder)
	}

	onChange: any = (value: any) => {
		this.input.setValue(value);
		this.onChangeCallback(this.input.value);
		this.onTouchedCallback();
	}

	onChangeCallback = (v: any) => {
	};
	onTouchedCallback = () => {
	};


	writeValue(value: any) {
		this.input.setValue(value)
	}

	registerOnChange(fn: any): void {
		this.onChangeCallback = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouchedCallback = fn;
	}

}
