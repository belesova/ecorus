import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
	selector: 'app-card-page',
	templateUrl: './card-page.component.html',
	styleUrls: ['./card-page.component.sass']
})
export class CardPageComponent {
	@Input() title: string = '';
	@Input() subtitle: string = '';
	@Input() img: string = '';
	@Input() link: string = ''

	constructor(private sanitizer: DomSanitizer, private router: Router) {
		sanitizer.bypassSecurityTrustHtml(this.title)
	}

	redirect() {
		this.router.navigate([this.link])
	}
}
