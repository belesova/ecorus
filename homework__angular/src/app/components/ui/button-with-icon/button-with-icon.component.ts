import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-button-with-icon',
	templateUrl: './button-with-icon.component.html',
	styleUrls: ['./button-with-icon.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonWithIconComponent {
	@Input() text!: string;
	@Input() icon!: string;
	@Input() size: string = '24px';
	@Input() columnGap: '4' | '8' = '4';

	constructor(private sanitizer: DomSanitizer) {
		sanitizer.bypassSecurityTrustHtml(this.text)
	}
}
