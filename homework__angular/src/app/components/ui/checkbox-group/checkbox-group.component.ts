import { Component, ChangeDetectionStrategy, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-checkbox-group',
	templateUrl: './checkbox-group.component.html',
	styleUrls: ['./checkbox-group.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxGroupComponent implements OnInit {
	@Input() arrCheckboxesInfo!: Array<any>;
	@Input() filterGroupTitle: string = '';
	@Input() hasSelectAll: boolean = false;

	form: FormGroup;

	constructor(private formBuilder: FormBuilder) {
		this.form = this.formBuilder.group({
			'selects': new FormArray([]),
			'selectAll': false
		})

		this.form.controls['selectAll'].valueChanges.subscribe(selectAll => {
			const arr = <FormArray>this.form.controls['selects']
			for (let i = 0; i < arr.length; i++) {
				arr.controls[i].setValue(selectAll)
			}
		})
	}

	ngOnInit(): void {
		this.arrCheckboxesInfo.forEach(value => {
			(this.getSelectsInfo()).push(this.formBuilder.control(false))
		})
	}

	getSelectsInfo() {
		return this.form.controls['selects'] as FormArray;
	}
}
