import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-card-product',
	templateUrl: './card-product.component.html',
	styleUrls: ['./card-product.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardProductComponent {
	@Input() brand!: string
	@Input() title!: string
	@Input() posterImg!: string
	@Input() category!: string
	@Input() cost!: number

	constructor(private sanitizer: DomSanitizer) {
		sanitizer.bypassSecurityTrustHtml(this.title)
		sanitizer.bypassSecurityTrustHtml(this.category)
	}
}
