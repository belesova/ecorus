import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-history-card',
	templateUrl: './history-card.component.html',
	styleUrls: ['./history-card.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoryCardComponent {
	@Input() material!: string;
	@Input() date!: string;
	@Input() address!: string;
	@Input() cost!: string;

	constructor(private sanitizer: DomSanitizer) {
		sanitizer.bypassSecurityTrustHtml(this.address)
		sanitizer.bypassSecurityTrustHtml(this.date)
		sanitizer.bypassSecurityTrustHtml(this.material)
		sanitizer.bypassSecurityTrustHtml(this.cost)
	}
}
