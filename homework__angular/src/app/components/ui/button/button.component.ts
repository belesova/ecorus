import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-button',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent {
	@Input() disabled: boolean = false;
	@Input() type: string = 'submit';
	@Input() text!: string;
	@Input() status: string = 'primary'
	@Input() width: number = 0;
	@Input() borderRadius: number = 8;
	@Input() paddings: '10' | '10-20' | '12' | '12-20' | '16' | '18-20' = '16';
	@Output() onClick = new EventEmitter<any>();

	constructor(private sanitizer: DomSanitizer) {
		sanitizer.bypassSecurityTrustHtml(this.text)
	}

	clickHandler(event: any) {
		this.onClick.emit(event)
	}
}
