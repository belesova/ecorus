import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { collectionPoint, CollectionPointsService } from '../../services/collection-points.service';

@Component({
	selector: 'app-cards-list-collection-points',
	templateUrl: './cards-list-collection-points.component.html',
	styleUrls: ['./cards-list-collection-points.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardsListCollectionPointsComponent {
	collectionPoints: Array<collectionPoint> = this.collectionPointsService.collectionPoints;

	constructor(private route: ActivatedRoute, private router: Router, private collectionPointsService: CollectionPointsService) {
	}

	addIdToQueryParams() {
		this.router.navigate([], {
			relativeTo: this.route,
			queryParams: {
				idCollectionPointCard: '123'
			}
		})
	}
}
