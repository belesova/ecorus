import { Component, ChangeDetectionStrategy } from '@angular/core';
import { DialogService } from '../../services/dialog.service';
import { QRModalComponent } from '../modals/qrmodal/qrmodal.component';

@Component({
	selector: 'app-balance-card',
	templateUrl: './balance-card.component.html',
	styleUrls: ['./balance-card.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class BalanceCardComponent {

	constructor(private dialogService: DialogService) {
	}

	openQRDialog(event: any) {
		this.dialogService.openDialog(QRModalComponent)
		event.stopPropagation()
	}
}
