import { Component, ChangeDetectionStrategy } from '@angular/core';
import { DialogService } from '../../services/dialog.service';
import { AuthFormComponent } from '../forms/auth-form/auth-form.component';

@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent {
	userAuth = window.localStorage.getItem('token');

	constructor(private dialogService: DialogService) {
	}

	closeModal() {
		this.dialogService.closeDialog()
	}

	openModal() {
		this.dialogService.openDialog(AuthFormComponent)
	}
}
