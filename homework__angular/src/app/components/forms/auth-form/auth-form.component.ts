import { Component, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogService } from '../../../services/dialog.service';
import { UserService } from '../../../services/user.service';
import { RegistrationFormComponent } from '../registration-form/registration-form.component';
import { AuthSmsFormComponent } from '../auth-sms-form/auth-sms-form.component';
import { AuthForCompanyComponent } from '../auth-for-company/auth-for-company.component';
import { numberValidator } from '../../../validators/NumberValidator';

@Component({
	selector: 'app-auth-form',
	templateUrl: './auth-form.component.html',
	styleUrls: ['../forms.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthFormComponent {
	form!: FormGroup;

	constructor(private formBuilder: FormBuilder, private dialogService: DialogService, private userService: UserService) {
		this.form = this.formBuilder.group({
			phone: ['', [Validators.required, numberValidator]],
			password: ['', Validators.required]
		})
	}

	closeDialog() {
		this.dialogService.closeDialog()
	}

	auth(form: FormGroup) {
		(this.userService.auth({
			password: form.controls['password'].value,
			phone: form.controls['phone'].value
		})).subscribe()
			.unsubscribe()
	}

	openSignUpModal = () => {
		this.closeDialog()
		this.dialogService.openDialog(RegistrationFormComponent)
	}

	openSignInWithSmsModal = () => {
		this.closeDialog()
		this.dialogService.openDialog(AuthSmsFormComponent)
	}

	openSignInCompany = () => {
		this.closeDialog()
		this.dialogService.openDialog(AuthForCompanyComponent)
	}
}
