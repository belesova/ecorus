import { Component, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogService } from '../../../services/dialog.service';
import { AuthFormComponent } from '../auth-form/auth-form.component';

@Component({
	selector: 'app-auth-for-company',
	templateUrl: './auth-for-company.component.html',
	styleUrls: ['../forms.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthForCompanyComponent {
	form!: FormGroup;

	constructor(private router: Router, private formBuilder: FormBuilder, private dialogService: DialogService) {
		this.form = this.formBuilder.group({
			companyName: ['', Validators.required],
			email: ['', [Validators.required, Validators.email]],
			password: ['', [Validators.required, Validators.min(3)]]
		})
	}

	auth(form: FormGroup) {
		//
	}

	openSignInModal = () => {
		this.closeDialog()
		this.dialogService.openDialog(AuthFormComponent)
	}

	closeDialog() {
		this.dialogService.closeDialog()
	}
}
