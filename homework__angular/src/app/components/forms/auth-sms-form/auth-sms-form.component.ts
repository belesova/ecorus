import { Component, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogService } from '../../../services/dialog.service';
import { UserService } from '../../../services/user.service';
import { AuthFormComponent } from '../auth-form/auth-form.component';
import { AuthForCompanyComponent } from '../auth-for-company/auth-for-company.component';
import { numberValidator } from '../../../validators/NumberValidator';

@Component({
	selector: 'app-auth-sms-form',
	templateUrl: './auth-sms-form.component.html',
	styleUrls: ['../forms.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthSmsFormComponent {
	step: number = 1;
	form!: FormGroup;

	constructor(private router: Router,
				private formBuilder: FormBuilder,
				private dialogService: DialogService,
				private userService: UserService) {
		this.form = this.formBuilder.group({
			phone: ['', [Validators.required, numberValidator]],
			code: ['', Validators.required]
		})
	}

	closeDialog() {
		this.dialogService.closeDialog()
	}

	auth = () => {
		if (this.step === 1) {
			this.step++
		} else {
			this.userService.registration({
				phone: this.form.controls['phone'].value,
				password: this.form.controls['code'].value
			})
		}
	}

	openSignInModal = () => {
		this.closeDialog()
		this.dialogService.openDialog(AuthFormComponent)
	}

	openSignInCompany = () => {
		this.closeDialog()
		this.dialogService.openDialog(AuthForCompanyComponent)
	}
}
