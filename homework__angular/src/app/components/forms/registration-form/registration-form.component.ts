import { Component, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogService } from '../../../services/dialog.service';
import { UserService } from '../../../services/user.service';
import { AuthFormComponent } from '../auth-form/auth-form.component';
import { Router } from '@angular/router';
import { AuthSmsFormComponent } from '../auth-sms-form/auth-sms-form.component';
import { AuthForCompanyComponent } from '../auth-for-company/auth-for-company.component';
import { numberValidator } from '../../../validators/NumberValidator';

@Component({
	selector: 'app-registration-form',
	templateUrl: './registration-form.component.html',
	styleUrls: ['../forms.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistrationFormComponent {
	form!: FormGroup;

	constructor(private router: Router,
				private formBuilder: FormBuilder,
				private dialogService: DialogService,
				private userService: UserService) {
		this.form = this.formBuilder.group({
			phone: ['', [Validators.required, numberValidator]],
			password: ['', Validators.required]
		})
	}

	signUp(form: FormGroup) {
		this.userService.registration({
			password: form.controls['password'].value,
			phone: form.controls['phone'].value
		}).subscribe()
			.unsubscribe()
	}

	closeDialog() {
		this.dialogService.closeDialog()
	}

	openSignInModal = () => {
		this.closeDialog()
		this.dialogService.openDialog(AuthFormComponent)
	}

	openSignInWithSmsModal = () => {
		this.closeDialog()
		this.dialogService.openDialog(AuthSmsFormComponent)
	}

	openSignInCompany = () => {
		this.closeDialog()
		this.dialogService.openDialog(AuthForCompanyComponent)
	}
}
