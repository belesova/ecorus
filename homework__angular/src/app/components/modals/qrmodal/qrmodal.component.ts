import { Component, ChangeDetectionStrategy } from '@angular/core';
import { DialogService } from '../../../services/dialog.service';

@Component({
	selector: 'app-qrmodal',
	templateUrl: './qrmodal.component.html',
	styleUrls: ['./qrmodal.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class QRModalComponent{
	constructor(private dialogService: DialogService) {
	}

	closeDialog() {
		this.dialogService.closeDialog()
	}
}
