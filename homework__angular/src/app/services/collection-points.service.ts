import { Injectable } from '@angular/core';

export interface collectionPoint {
	img: string,
	address: string,
	materials: string
}

@Injectable({
	providedIn: 'root'
})
export class CollectionPointsService {
	collectionPoints: Array<collectionPoint> = [
		{
			img: '/assets/img/CollectionPointCard.png',
			address: 'ул.Кремлёвская, 88',
			materials: 'Пластик, стекло, бумага, металл, старая одежда, батареи, аккумуляторы...'
		},
		{
			img: '/assets/img/CollectionPointCard.png',
			address: 'ул.Кремлёвская, 88',
			materials: 'Пластик, стекло, бумага, металл, старая одежда, батареи, аккумуляторы...'
		},
		{
			img: '/assets/img/CollectionPointCard.png',
			address: 'ул.Кремлёвская, 88',
			materials: 'Пластик, стекло, бумага, металл, старая одежда, батареи, аккумуляторы...'
		},
	]
}
