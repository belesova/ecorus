import { Injectable, TemplateRef } from '@angular/core';
import { Dialog, DialogConfig, DialogRef } from '@angular/cdk-experimental/dialog';
import { ModalContainerComponent } from '../components/modal-container/modal-container.component';
import { ComponentType } from '@angular/cdk/overlay';
import { BottomSheetComponent } from '../components/bottom-sheet/bottom-sheet.component';

@Injectable({
  providedIn: 'root'
})
export class BottomSheetService {
	constructor(private bottomSheet: Dialog) {
	}

	openDialog<T>(component: ComponentType<T>, config: DialogConfig = {}): DialogRef<ModalContainerComponent> {
		return this.bottomSheet.openFromComponent(component, {
			maxWidth: 'none',
			...config,
			containerComponent: BottomSheetComponent,
		});
	}

	closeDialog = () => {
		this.bottomSheet.closeAll()
	}
}
